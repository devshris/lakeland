package Constants;

import org.openqa.selenium.By;

public interface SearchConstant {

	By searchTextBox = By.cssSelector("#search");
	By searchButton = By.cssSelector(".btn.basket-btn");
}
