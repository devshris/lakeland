package Constants;

import org.openqa.selenium.By;

public interface RegisterConstant {

	By emailTextBox = 			By.cssSelector("#signup-email");
	By registerSubmitButton = 	By.cssSelector("#Registration_form-submit");
	By title = 					By.cssSelector("select[name='customerBean.title']");
	By firstName = 				By.xpath("//input[@id='customerBean.firstName']");
	By lastName = 				By.xpath("//input[@id='customerBean.lastName']");
	By date = 					By.cssSelector("#RegisterUser_ageVerificationBean_dayOfBirth");
	By month = 					By.cssSelector("#RegisterUser_ageVerificationBean_monthOfBirth");
	By year = 					By.cssSelector("#RegisterUser_ageVerificationBean_yearOfBirth");
	By telephone = 				By.xpath("//input[@id='customerBean.mobile']");
	By password = 				By.cssSelector("#password");
	By confirmPassword = 		By.cssSelector("#confirmPassword");
	By homeAddress = 			By.cssSelector("#address-postcode");
	By findAddressButton =		By.cssSelector("#lookupAddressJsBtn");
	By searchResultForAddress=	By.xpath("//option[contains(text(),'Flat 18 Acacia Court Alpine Road - London NW9')]");
	By checkBoxForConsent =  	By.cssSelector(".label-checkbox.ll-enabled");
	By registerButton = 		By.cssSelector("#RegisterUser_registerButton");
}
