package com.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

public class Action extends BaseClass{
	
	public void clickOnELement(By element) {
		
		driver.findElement(element).click();
		}
	
public void clickOnELement(By element, int index) {
		
		driver.findElements(element).get(index).click();
		}


	public By UpdateTextBox(By element, String search) {
		driver.findElement(element).clear();
		driver.findElement(element).sendKeys(search);
		return element;
	}
	
	public void dropDown(By element, int index) {
		
		Select sizeDropDown= new Select(driver.findElement(element));
		sizeDropDown.selectByIndex(index);
	}
	
public void dropDown(By element, String value) {
		
		Select sizeDropDown= new Select(driver.findElement(element));
		sizeDropDown.selectByValue(value);;
	}
}
