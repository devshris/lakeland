package com.runner;

import org.openqa.selenium.WebDriver;

import com.driver.Action;
import com.driver.Waits;
import com.pages.AccountPage;
import com.pages.HomePage;
import com.pages.LoginPage;
import com.pages.PostRegistrationPage;
import com.pages.RegisterPage;
import com.pages.SearchResultPage;

public class BaseClass {
	
	public static WebDriver driver;
	public static String globalSearchWord;
	public static Action action = new  Action();
	public static Waits waits = new Waits();
	
	public static HomePage homePage = new HomePage();
	public static SearchResultPage searchResultPage = new SearchResultPage();
	
	public static LoginPage loginPage = new LoginPage();
    public static AccountPage accountPage = new AccountPage();	
    
    public static RegisterPage registerPage = new RegisterPage();
    public static PostRegistrationPage postRegistrationPage = new PostRegistrationPage();

}
