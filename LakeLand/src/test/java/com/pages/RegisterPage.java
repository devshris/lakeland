package com.pages;

import java.util.Map;

import org.junit.Assert;

import com.runner.BaseClass;

import Constants.RegisterConstant;
import cucumber.api.DataTable;

public class RegisterPage extends BaseClass {
	
	public void verifyRegister() {
		
		action.UpdateTextBox(RegisterConstant.emailTextBox,"disha_0912@yahoo.com");
		action.clickOnELement(RegisterConstant.registerSubmitButton);
		Assert.assertEquals("https://www.lakeland.co.uk/OpenRegistration.action", driver.getCurrentUrl());
	}
	
	public void verifyValidDetailsForRegistration(DataTable validRegistrationTable) {
		
		
		action.dropDown(RegisterConstant.title, "MISS");
		
		Map <String, String> validRegistrationDetails = validRegistrationTable.asMap(String.class, String.class);
		
		String firstName = validRegistrationDetails.get("firstName");
		String lastName = validRegistrationDetails.get("lastName");
		String date =  validRegistrationDetails.get("date");
		String month =  validRegistrationDetails.get("month");
		String year =  validRegistrationDetails.get("year");
		String telephone =  validRegistrationDetails.get("telephone");
		String homeAddress =  validRegistrationDetails.get("homeAddress");
		String password =  validRegistrationDetails.get("password");
		String confirmpwd =  validRegistrationDetails.get("confirmPassword");
		
		action.UpdateTextBox(RegisterConstant.firstName, firstName);
		action.UpdateTextBox(RegisterConstant.lastName, lastName);
		action.UpdateTextBox(RegisterConstant.date, date);
		action.UpdateTextBox(RegisterConstant.month, month);
		action.UpdateTextBox(RegisterConstant.year, year);
		action.UpdateTextBox(RegisterConstant.telephone, telephone);
		action.UpdateTextBox(RegisterConstant.homeAddress, homeAddress);
		action.clickOnELement(RegisterConstant.findAddressButton);
		action.clickOnELement(RegisterConstant.searchResultForAddress);
		action.UpdateTextBox(RegisterConstant.password, password);
		action.UpdateTextBox(RegisterConstant.confirmPassword, confirmpwd);
		}

	public void verifyConsentCheckBox() {
		action.clickOnELement(RegisterConstant.checkBoxForConsent);
		
		action.clickOnELement(RegisterConstant.registerButton);//cannot run due to recaptcha
	}
}
