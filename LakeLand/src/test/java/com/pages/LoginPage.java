package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

import Constants.LoginConstant;

public class LoginPage extends BaseClass {
	
	public void verifyLoginIcon() {
		
		action.clickOnELement(LoginConstant.loginIcon);
		Assert.assertEquals("Lakeland sign in / registration", driver.getTitle());
	}

}
