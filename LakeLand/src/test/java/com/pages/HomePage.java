package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.runner.BaseClass;

import Constants.SearchConstant;

public class HomePage extends BaseClass{
	
	
public void verifyHomePage() {
	
	Assert.assertEquals("Lakeland | Cookware, Bakeware, Cleaning & Laundry", driver.getTitle());
	
	}

public void verifySearchForTheValidProduct(String search) throws InterruptedException {
	globalSearchWord=search;
	action.UpdateTextBox(SearchConstant.searchTextBox,search);
	action.clickOnELement(SearchConstant.searchButton, 0);
//	driver.findElements(SearchConstant.searchButton).get(0);

}

public void verifySearchForTheInvalidProduct(String search) {
  
	globalSearchWord=search;


action.UpdateTextBox(SearchConstant.searchTextBox,search);
action.clickOnELement(SearchConstant.searchButton, 0);

}

public void verifyLinkText() {
	
	Actions action = new Actions(driver);
	WebElement element = driver.findElement(By.linkText("Cooking"));
	action.moveToElement(element).build().perform();
	
	//selecting Tableware
	
	WebElement element2 = driver.findElement(By.linkText("Tableware"));
	action.click(element2).build().perform();
}
}
