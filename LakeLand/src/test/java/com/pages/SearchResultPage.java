package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;


public class SearchResultPage extends BaseClass {
	
	public void verifyValidSearchResults() {
		
		Assert.assertEquals(globalSearchWord, driver.findElement(By.cssSelector("h1")).getText());
		
		}
	
	public void verifyInvalidSearchResults() {
		
		Assert.assertEquals("NO SEARCH RESULTS WERE FOUND FOR "+globalSearchWord, driver.findElements(By.cssSelector(".m-bottom")).get(0).getText());
		
	
	}
	
	public void verifyLinkTextResults() {
		
		Assert.assertEquals("Tableware | Dining & Entertaining | Lakeland", driver.getTitle());
	}

}
