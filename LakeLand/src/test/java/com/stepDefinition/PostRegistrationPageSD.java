package com.stepDefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class PostRegistrationPageSD extends BaseClass {
	
	@Then("^I should be able to register successfully$")
	public void i_should_be_able_to_register_successfully() throws Throwable {
		
		postRegistrationPage.verifyPostSignUpPage();
	   }


}
