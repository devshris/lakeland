package com.stepDefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class LoginPageSD extends BaseClass {
	
	@Given("^I am on Login Page$")
	public void i_am_on_Login_Page() throws Throwable {
		
		loginPage.verifyLoginIcon();
	   
	}

	@When("^I enter valid login credentials$")
	public void i_enter_valid_login_credentials() throws Throwable {
	   }


	@When("^I enter invalid login credentials$")
	public void i_enter_invalid_login_credentials() throws Throwable {
	   }


}
