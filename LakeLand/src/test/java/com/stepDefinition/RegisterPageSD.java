package com.stepDefinition;

import com.runner.BaseClass;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class RegisterPageSD extends BaseClass {
	
	@Given("^I am on Register Page$")
	public void i_am_on_Register_Page() throws Throwable {
		
		loginPage.verifyLoginIcon();
		registerPage.verifyRegister();
	}

	@When("^I enter all the valid details$")
	public void i_enter_all_the_valid_details(DataTable validRegistrationTable) throws Throwable {
		registerPage.verifyValidDetailsForRegistration(validRegistrationTable);
		}
	
	@When("^I select the consent statement checkbox$")
	public void i_select_the_consent_statement_checkbox() throws Throwable {
		registerPage.verifyConsentCheckBox();
	}

	


}
