package com.stepDefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class SearchResultPageSD extends BaseClass {

	@Then("^I should see related results$")
	public void i_should_see_related_results() throws Throwable {

		searchResultPage.verifyValidSearchResults();

	}

	@Then("^I should see a message$")
	public void i_should_see_a_message() throws Throwable {

		searchResultPage.verifyInvalidSearchResults();

	}

	@Then("^I should be able to select from drop down list$")
	public void i_should_be_able_to_select_from_drop_down_list() throws Throwable {

		searchResultPage.verifyLinkTextResults();

	}

}
