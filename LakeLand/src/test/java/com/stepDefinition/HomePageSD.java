package com.stepDefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class HomePageSD extends BaseClass {
	
	@Given("^I am on the Home page$")
	public void i_am_on_the_Home_page() throws Throwable {
		homePage.verifyHomePage();
		
	}
	@When("^I enter valid \"([^\"]*)\" data$")
	public void i_enter_valid_data(String search) throws Throwable {
		homePage.verifySearchForTheValidProduct(search);
	}

	@When("^I enter invalid \"([^\"]*)\" data$")
	public void i_enter_invalid_data(String search) throws Throwable {
		homePage.verifySearchForTheInvalidProduct(search);
	    }

	@When("^I hover on the link text$")
	public void i_hover_on_the_link_text() throws Throwable {
		homePage.verifyLinkText();
	    	}

	


}
