@smoke
Feature:  Login Functionality

Scenario: verify login with valid data

Given I am on Login Page
When I enter valid login credentials
Then I should be able to login successfully


Scenario: verify login with invalid data

Given I am on Login Page
When I enter invalid login credentials
Then I should get an error message


