Feature: Search Functionality

Background: 

Given I am on the Home page

Scenario Outline: to verify search with valid data


When I enter valid "<search>" data 
Then I should see related results

Examples: 
|search|
|Plate|
|Baking|
|Colourworks|


Scenario Outline: to verify search with invalid data

When I enter invalid "<search>" data
Then I should see a message

Examples: 
|search|
|JHAHSIAUDINKSSJD|
|PRAYING|
|1111111111|
|NEWLOOK|

Scenario: to verify search with link text

When I hover on the link text
Then I should be able to select from drop down list
