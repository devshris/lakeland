Feature: Register Functionality

@test
Scenario: verify register functionality

Given I am on Register Page

When I enter all the valid details
|firstName|Disha|
|lastName|Singh|
|date|12|
|month|10|
|year|1987|
|telephone|12345678|
|homeAddress|NW9 9BP|
|password|Disha12.|
|confirmPassword|Disha12.|

And I select the consent statement checkbox

Then I should be able to register successfully


